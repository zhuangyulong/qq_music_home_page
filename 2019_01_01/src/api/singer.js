// 歌手页面的api封装方法
import jsonp from 'common/js/jsonp'
import originJSONP from 'jsonp'
import {
  commonParams,
  options
} from './config'

export const AAA = 'asd';
// 获取歌手数据
export function getSingerList() {
  let url = "https://u.y.qq.com/cgi-bin/musicu.fcg?callback=getUCGI6721266343477472&g_tk=5381&jsonpCallback=getUCGI6721266343477472&loginUin=0&hostUin=0&format=jsonp&inCharset=utf8&outCharset=utf-8&notice=0&platform=yqq&needNewCode=0&data=%7B%22comm%22%3A%7B%22ct%22%3A24%2C%22cv%22%3A10000%7D%2C%22singerList%22%3A%7B%22module%22%3A%22Music.SingerListServer%22%2C%22method%22%3A%22get_singer_list%22%2C%22param%22%3A%7B%22area%22%3A-100%2C%22sex%22%3A-100%2C%22genre%22%3A-100%2C%22index%22%3A-100%2C%22sin%22%3A0%2C%22cur_page%22%3A1%7D%7D%7D";
  const options = {
    param: ''
  }
  return new Promise((resolve, reject) => {
    originJSONP(url, options, (err, data) => {
      if (!err) {
        resolve(data)
      } else {
        reject(err)
      }
    })
  })
  // var singerlist = [];
  // originJSONP(url, options, (err, data) => {
  //   if (!err) {
  //     console.log(data.singerList.data.singerlist);
  //     var singerlist = data.singerList.data
  //     // singerlist.push('aa');
  //   } else {
  //     console.log("获取歌手数据api错误！！")
  //   }
  // })
  // return singerlist;
}
