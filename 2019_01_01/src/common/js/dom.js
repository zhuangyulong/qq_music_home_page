// 为组件添加样式函数
// 为组件元素添加样式
export function addClass(el, className){
  if (hasClass(el, className)) { //判断这个对象有这个classname的时候
    return
  }
  // 添加classname的思路：1，拆分一个数组；2，添加进新的classname；3，合并为一个新的数组
  let newClass = el.className.split(' ');
  newClass.push(className);
  el.className = newClass.join(' ');
}
// 判断组件元素是否含有该样式
export function hasClass(el, className) {
  let reg = new RegExp('(^|\\s)' + className + '(\\s|$)'); //定义一个正则用于判断
  return reg.test(el.className); //判断
}
// setAttribute() 方法添加指定的属性，并为其赋指定的值。
export function getData(el, name, val) {
  const prefix = 'data-'
  if (val) {
    return el.setAttribute(prefix + name, val)
  }
  return el.getAttribute(prefix + name)
}