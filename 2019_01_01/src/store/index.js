// 入口文件
import Vue from 'vue'
import Vuex from 'vuex'
import * as actions from './actions' //* as 这种语法，就可以通过actions加点取到所需要的参数
import * as getters from './getters'
import state from './state'
import mutations from './mutations'
import createLogger from 'vuex/dist/logger' //在控制台中打印出来日志

Vue.use(Vuex);

// vuex自带的调试工具
const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  actions,
  getters,
  state,
  mutations,
  strict: debug,
  plugins: debug ? [createLogger()] : []
})
